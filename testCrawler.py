#!/usr/local/bin/python3.6 python3
# _*_ coding:utf-8 _*_
import requests
from bs4 import BeautifulSoup
import re
import time

def readText(url_list):
    for info in url_list:
        time.sleep(0.5)
        reqt = requests.get("https://www.boquge.com" + info.get("href"))
        print(info.get("href"))
        # print(reqt.encoding)
        # reqt.encoding = "utf-8"
        html = reqt.text
        soupt = BeautifulSoup(html, "html.parser")
        title = str(soupt.title.text.split("_")[0])
        text = str(soupt.find(id="txtContent").text)
        f = open("test.txt", "a+", encoding="utf-8")
        f.write(title.strip() + "\n")
        f.write(text.strip() + "\n\n\n\n")
    f.close()


if __name__ == "__main__":
    url = 'https://www.boquge.com/book/46773/'
    req = requests.get(url)
    # req.encoding = "utf-8"
    html = req.text
    soup = BeautifulSoup(html, "html.parser")
    # print(soup)
    list = soup.find_all("a", href=re.compile(u'/book/46773/\d{0,}\.html'))
    readText(list)
    s = requests.session()
    s.keep_alive = False

#!/usr/bin/env python
# -*- coding: utf-8 -*-
__mtime__ = '2018/4/16'
import pymysql
import pymysql.cursors

connection = pymysql.connect(host="localhost",
                             user="root",
                             password="root",
                             db="test",
                             port=3306,
                             charset="utf8")

connection2 = pymysql.connect(host="localhost",
                              user="root",
                              password="root",
                              db="information_schema",
                              port=3306,
                              charset="utf8")


class Mysql(object):
    def do_select(self):
        try:
            with connection2.cursor() as cursor2:
                sql = "select COLUMN_NAME from COLUMNS where TABLE_NAME='test_beans_' and TABLE_SCHEMA='test'"
                cursor2.execute(sql)
                for row in cursor2.fetchall():
                    print(row[0], end="\t")
                connection2.commit()

            with connection.cursor() as cursor:
                sql = 'select * from test_beans_'
                cursor.execute(sql)
                for row in cursor.fetchall():
                    print()
                    for r in row:
                        print(r, end="\t")
                connection.commit()
        finally:
            connection.close()
            connection2.close()


if __name__ == "__main__":
    Mysql().do_select()

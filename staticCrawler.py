#!/usr/local/bin/python3.6 python3
# _*_ coding:utf-8 _*_
import requests
from bs4 import BeautifulSoup
import time
import os


class read_notice(object):
    def __init__(self):
        self.url = "http://www.cjmsa.gov.cn"
        self.list_url = "http://www.cjmsa.gov.cn/vcms/classArticleListByPage.do?type=active&channelId=5&classIds=26&templateId=300&page="
        self.headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36'}
        self.path = os.getcwd() + "/file/"

    def read_title(self):
        for i in range(63):
            pageUrl = self.list_url + str(i + 1)
            req = requests.post(pageUrl, headers=self.headers)
            req.encoding = "utf-8"
            html = req.text
            soup = BeautifulSoup(html, "html.parser")
            list = soup.find_all("li", class_="hui3")
            for l in list:
                time.sleep(0.5)
                href = l.find("a").get("href")
                self.read_text(href)

    def read_text(self, href):
        url = self.url + href
        req = requests.get(url, headers=self.headers)
        req.encoding = "utf-8"
        html = req.text
        soup = BeautifulSoup(html, "html.parser")
        title = soup.find(id="artibodyTitle").text
        content = soup.find(class_="article replaceArticle")
        if content.find("a") and content.find("a").get("href"):
            self.dl_file(content.find("a").get("href"), title)
        else:
            self.wt_file(content.text, title)

    def dl_file(self, path, title):
        url = self.url + path
        stuffix = path.split(".")[1]
        req = requests.get(url, headers=self.headers)
        print("下载：%s，地址是：%s" % (title, path))
        with open(self.path + str(title).strip() + "." + stuffix, "wb") as code:
            code.write(req.content)

    def wt_file(self, content, title):
        print("下载：%s，这是一个文本" % title)
        text = str(content).strip()
        with open(self.path + str(title).strip() + ".txt", "wt", encoding="utf-8") as code:
            code.write(text)


if __name__ == "__main__":
    read_notice().read_title()
